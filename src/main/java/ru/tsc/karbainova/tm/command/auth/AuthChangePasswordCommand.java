package ru.tsc.karbainova.tm.command.auth;

import ru.tsc.karbainova.tm.command.AuthAbstractCommand;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class AuthChangePasswordCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "change-password";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change Password";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter new password:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);

    }
}
