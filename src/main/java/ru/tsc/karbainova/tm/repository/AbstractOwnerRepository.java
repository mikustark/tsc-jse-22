package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.IOwnerRepository;
import ru.tsc.karbainova.tm.model.AbstractEntity;
import ru.tsc.karbainova.tm.model.AbstractOwnerEntity;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {
    protected final List<E> entities = new ArrayList<>();

    @Override
    public void remove(String userId, E entity) {
        if (!userId.equals(entity.getUserId())) return;
        entities.remove(entity);
    }

    @Override
    public void clear(String userId) {
        findAll(userId).forEach(o -> entities.remove(o.getUserId()));
    }

    public List<E> findAll(String userId) {
        return entities.stream().filter(o -> userId.equals(o.getUserId()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public List<E> findAll(String userId, Comparator<E> comparator) {
        return findAll(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public void add(String userId, E entity) {
        entity.setUserId(userId);
        entities.add(entity);
    }

    @Override
    public E findById(String userId, String id) {
        if (id == null) return null;
        for (E entity : entities) {
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E removeById(String userId, String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }
}
